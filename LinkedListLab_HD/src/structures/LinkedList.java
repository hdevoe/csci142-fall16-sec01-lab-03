package structures;

/**
 *  A class to provide the basic methods of a singly linked list.
 *  Note that it is simplified in that it does not implement or extend
 *  List, Collection, etc.  It is for learning the basics of Linked
 *  Lists.
 *
 *  @author Daniel Plante
 *  @version 1.0   2 March 2002
 *  @version 1.1   19 November 2013
 */
public class LinkedList<E>
{
    /////////////////////////////
    //         Properties      //
    /////////////////////////////
    private Node<E> myHead;
    private int mySize;
    
    /////////////////////////////
    //         Methods         //
    /////////////////////////////
    
    /**
     *  Default constructor that creates an empty linked list
     *
     *  <pre>
     *  pre:  the linked list is empty
     *  post: the linked list is empty
     *  </pre>
     */
    public LinkedList()
    {
        myHead = null;
    }
    
    /**
     *  Constructor that creates a new linked list with a single 
     *  node storing the object passed in
     *
     *  <pre>
     *  pre:  myHead points to null (the linked list is empty)
     *  post: myHead points to the only node in the linked list,
     *        that node holding the object passed in
     *  </pre>
     *
     *  @param datum an object to be inserted at the head of the
     *         linked list
     */
    public LinkedList(E datum)
    {
        myHead = new Node<E>(datum);
        myHead.setNext(null);
    }
    
    /**
     *  Adds a node to the head of the linked list; the special
     *  condition of an empty linked list is handled without
     *  special treatment since if myHead points to null, that
     *  simply becomes the next node in the list, immediately
     *  following the new entered node at the head of the list
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the beginning of the list
     *  </pre>
     *
     *  @param aNode the node to be entered
     */
    private void addFirst(Node<E> aNode)
    {
        aNode.setNext(myHead);
        myHead = aNode;
    }
    
    /**
     *  Adds a node to the head of the linked list; the special
     *  condition of an empty linked list is handled without
     *  special treatment since if myHead points to null, that
     *  simply becomes the next node in the list, immediately
     *  following the new entered node at the head of the list
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the beginning of the list
     *  </pre>
     *
     *  @param datum the object used to create a new node to be 
     *         entered at the head of the list
     */
	public void addFirst(E datum)
    {
        Node<E> aNode;
        
        aNode = new Node<E>(datum);
        this.addFirst(aNode);
    }
    
    /**
     *  Adds a node to the tail of the linked list; the special
     *  condition of an empty linked list is handled separately
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the end of the list
     *  </pre>
     *
     *  @param aNode the node to be entered
     */
    private void addLast(Node<E> aNode)
    {
        Node<E> lastNode;
        
        if(myHead==null)
        {
            this.addFirst(aNode);
        }
        else
        {
            lastNode = this.getPrevious(null);
            lastNode.setNext(aNode);
            aNode.setNext(null);
        }
    }
    
    /**
     *  Adds a node to the tail of the linked list; the special
     *  condition of an empty linked list is handled separately
     *
     *  <pre>
     *  pre:  the linked list may be empty or contain one or
     *        more nodes
     *  post: the linked list contains one more node that has
     *        been added to the end of the list
     *  </pre>
     *
     *  @param datum the object used to creat a new node to be 
     *         entered at the tail of the list
     */
    public void addLast(E datum)
    {
        Node<E> aNode;
        
        aNode = new Node<E>(datum);
        this.addLast(aNode);
    }
    
    /**
     *  Deletes a node from the list if it is there
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: if the node to be deleted is in the list,
     *        the node no longer exists in the list; the
     *        node previous to the node to be deleted now
     *        points to the node following the deleted node
     *  </pre>
     *
     *  @param aNode the node to be deleted from the list
     *
     *  @return boolean indicating whether or not the node
     *          was deleted
     */
    private boolean remove(Node<E> aNode)
    {
    	Node<E> currentNode;
        currentNode = myHead;
        
        
        while(currentNode != null)
        {
            if(aNode.equals(myHead))
            {
            	myHead = currentNode.getNext();
                return true;
            } else if(currentNode.equals(aNode))
            {
                getPrevious(currentNode).setNext(currentNode.getNext());
                return true;
            }
            currentNode = currentNode.getNext();
        }
        return false;
    }
    
    /**
     *  Deletes a node from the list if it is there
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: if the node to be deleted is in the list,
     *        the node no longer exists in the list; the
     *        node previous to the node to be deleted now
     *        points to the node following the deleted node
     *  </pre>
     *
     *  @param datum the object to be deleted from the list
     *
     *  @return boolean indicating whether or not the node
     *          was deleted
     */
    public boolean remove(E datum)
    {
    	Node<E> currentNode;
        E currentDatum;
        
        currentNode = myHead;
        currentDatum = null;
        
        
        while(currentNode != null)
        {
            currentDatum = currentNode.getData();
            if(currentDatum.equals(datum) && currentNode.equals(myHead))
            {
                myHead = currentNode.getNext();
                return true;
            } else if(currentDatum.equals(datum))
            {
                getPrevious(currentNode).setNext(currentNode.getNext());
                return true;
            }
            currentNode = currentNode.getNext();
        }
        return false;
    }
    
    /**
     *  Find a node in the list with the same data as that passed in 
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: list is unchanged
     *  </pre>
     *
     *  @param datum the object for which a node is to be found 
     *         in the list
     *
     *  @return null if a node with the given object datum is not in
     *          the list, or the node if it does
     */
    private Node<E> findNode(E datum)
    {
        Node<E> currentNode;
        E currentDatum;
        
        currentNode = myHead;
        currentDatum = null;
        
        while(currentNode != null)
        {
            currentDatum = currentNode.getData();
            if(currentDatum.equals(datum))
            {
                return currentNode;
            }
            currentNode = currentNode.getNext();
        }
        return null;
    }
    
    /**
     *  Determine if a node exists in the list with the same 
     *  data as that passed in 
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: list is unchanged
     *  </pre>
     *
     *  @param datum the object for which a node is to be found 
     *         in the list
     *
     *  @return false if a node with the given object datum is not in
     *          the list, or true if it does
     */
	public boolean contains(E datum)
    {

		if (datum == null) return false;
		
        Node<E> currentNode;
        E currentDatum;
        
        currentNode = myHead;
        currentDatum = null;
        
        while(currentNode != null)
        {
            currentDatum = currentNode.getData();
            if(currentDatum.equals(datum))
            {
                return true;
            }
            currentNode = (Node<E>)currentNode.getNext();
        }
        return false;
    }
    
    /**
     *  Determines the node that resides one closer to the
     *  head of the list than the node passed in
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: the list is unchanged
     *  </pre>
     *
     *  @param aNode the node whose predecessor is being looked for
     *
     *  @return the node that resides one closer to the head of the
     *          list than the node passed in
     */
    private Node<E> getPrevious(Node<E> aNode)
    {
        Node<E> currentNode;
        
        currentNode = myHead;
        
        if(currentNode == aNode)
        {
            return null;
        }
        
        while(currentNode!=null && currentNode.getNext()!=aNode)
        {
            currentNode = currentNode.getNext();
        }
        
        return currentNode;
    }
    
    /**
     *  A new node is entered into the list immediately before
     *  the designated node
     *
     *  <pre>
     *  pre:  the list may have 0 or more nodes in it
     *  post: if the beforeNode is not in the list, no change
     *        takes place to the list; otherwise, the new
     *        node is entered in the appropriate place
     *  </pre>
     *
     *  @param aNode the node to be entered into the list
     *  @param beforeNode the node before which the new node
     *         is to be entered
     *
     *  @return boolean designating if the node was or was not
     *          entered into list
     */
    private boolean insertBefore(Node<E> aNode, Node<E> beforeNode)
    {
    	 Node<E> currentNode;
         
         currentNode = myHead;
         
         if(currentNode == beforeNode)
         {
             addFirst(aNode);
             return true;
         }
         

         while(currentNode!=null && currentNode.getNext()!=beforeNode)
         {
             currentNode = currentNode.getNext();
         }
         
         if (currentNode!=null)
         {
        	 currentNode.setNext(aNode);
        	 aNode.setNext(beforeNode);
        	 return true;
         }
         
         return false;
    }
    
    /**
     *  A new node with datum is entered into the list immediately
     *  before the node with beforeDatum, the designated node
     *
     *  <pre>
     *  pre:  the list may have 0 or more nodes in it
     *  post: if the node with beforeDatum is not in the list, 
     *        no change takes place to the list; otherwise, a new
     *        node is entered in the appropriate place with the 
     *        object datum
     *  </pre>
     *
     *  @param datum the object used to create the new node 
     *         to be entered into the list
     *  @param beforeDatum the datum of the node before which the 
     *         new node is to be entered
     *
     *  @return boolean designating if the node was or was not
     *          entered
     */
    public boolean insertBefore(E datum,E beforeDatum)
    {
    	
    	
    	
    	Node<E> currentNode;
        currentNode = myHead;
        
        if (myHead == null)
        {
        	Node<E> newNode = new Node<E>(datum);
        	setHead(newNode);
        	return true;
        }
        
        if (!contains(beforeDatum) && beforeDatum != null) return false;
        
        
        if(currentNode.getData().equals(beforeDatum))
        {
        	Node<E> newNode = new Node<E>(datum);
        	addFirst(newNode);
        	return true;
        }
        
        
        while(currentNode!=null)
        {
        	if (!(currentNode.getNext() == null))
        	{
        		if (!currentNode.getNext().getData().equals(beforeDatum))
        		{
        			currentNode = currentNode.getNext();
        		} else break;
        	}else break;
        }
        
        if (currentNode!=null)
        {
        	Node<E> newNode = new Node<E>(datum);
        	newNode.setNext(currentNode.getNext());
       	 	currentNode.setNext(newNode);
       	 	return true;
        }
        
        return false;
    }
    
    /**
     *  print the list by converting the objects in the list
     *  to their string representations
     *
     *  <pre>
     *  pre:  the list has 0 or more elements
     *  post: no change to the list
     *  </pre>
     */
    public String toString()
    {
        String string;
        Node<E> currentNode;
        
        currentNode = myHead;
        
        string = "head ->";
        
        while(currentNode!=null)
        {
            string += currentNode.getData().toString()+ " -> ";
            currentNode = currentNode.getNext();
        }
        string += "|||";
        return string;
    }

    
    /**
     * Returns the index of the given datum
     * @author hudson
     * @param o
 	 * @return
 	 */
    public int indexOf(E o)
    {
    	  Node<E> currentNode;
          E currentDatum;
          
          int index = 0;
          
          currentNode = myHead;
          currentDatum = null;
          
          while(currentNode != null)
          {
              currentDatum = currentNode.getData();
              if(currentDatum.equals(o))
              {
                  return index;
              }
              currentNode = (Node<E>)currentNode.getNext();
              index++;
          }
          return -1;
    } 

    /**
     * Removes the first node from the list, making the next node in line the new head
     * @return
     */
    public E removeFirst()
    {
    	if (myHead != null)
    	{
    		E firstData = myHead.getData();
    		myHead = myHead.getNext();
    		return firstData;
    	}
    	return null;
    }
    
    /**
     * Removes the last node from the list
     * @author hudson
     * @return
     */
    public E removeLast()
    {
    	if (!(size()<=0))
    	{
    		Node<E>lastNode = getPrevious(null);
    		if (getPrevious(lastNode) != null)
    		{
    			getPrevious(lastNode).setNext(null);
    		} else 
    		{
    			remove(lastNode);
    		}
    		return lastNode.getData();
    	}
    	return null;
    }

    /**
     * Returns the size of the Linked List
     * @return
     */
    public int size()
    {
    	Node<E> currentNode;
    	currentNode = myHead;
        int size = 0;
        while(currentNode != null)
        {
            currentNode = (Node<E>)currentNode.getNext();
            size++;
        }
        return size;
    }

    /**
     * Returns the first node in the linked list (the head)
     * @author hudson
     * @return
     */
    public E getFirst()
    {
        if (myHead != null)
        {
        	return myHead.getData();
        }
        return null;
    }
    
    /**
     * Returns the last node in the linked list
     * @return
     */
    public E getLast()
    {
        if (getPrevious(null) != null)
        {
        	return getPrevious(null).getData();
        }
        return null;
    }

    /**
     * Removes the given node from the list and places it as the new head
     * @author hudson
     * @param o
     */
    public void setFirst(E o)
    {
    	if (contains(o))
    	{
    		Node<E> foundNode = findNode(o);
    		remove(findNode(o));
    		addFirst(foundNode.getData());
    	}
    }
    
    /**
     * Sets the head of the linked list to the given node, making the previous head the next node 
     * in line
     * @param aNode
     */
    private void setHead(Node<E> aNode)
    {
        myHead = aNode;
    }
    
    /**
     * Returns the head of the linked list
     * @author hudson
     * @return
     */
    private Node<E> getHead()
    {
        return myHead;
    }
    
    
    
    private class Node<T>
    {
        ///////////////////////////////////
        //           Properties          //
        ///////////////////////////////////
        private T myData;
        private Node<T> myNext;
        
        ///////////////////////////////////
        //             Methods           //
        ///////////////////////////////////
        
        /**
         *  Default constructor for a node with null
         *  data and pointer to a next node
         */
        public Node()
        {
            myData = null;
            myNext = null;
        }
        
        /**
         *  Constructor for a node with some object for
         *  its data and null for a pointer to a next node
         *
         *  <pre>
         *  pre:  a null node
         *  post: a node with some object for its data and
         *        null for a pointer to a next node
         *  </pre>
         *
         *  @param datum an object for the node's data
         */
        public Node(T datum)
        {
            myData = datum;
            myNext = null;
        }
        
        /**
         *  Constructor for a node with some object for 
         *  its data and a pointer to another node
         *
         *  <pre>
         *  pre:  a null node
         *  post: a node with some object for its data and
         *        a pointer to a next node
         *  </pre>
         *
         *  @param datum an object for the node's data
         *  @param next the node that this node points to
         */
        public Node(T datum, Node<T> next)
        {
            myData = datum;
            myNext = next;
        }
        
        // Accessor methods
        public void setData(T datum)
        {
            myData = datum;
        }
        
        public T getData()
        {
            return myData;
        }
        
        public void setNext(Node<T> next)
        {
        	if (next == null)
        	{
        		myNext = null;
        	} else myNext = next;
        }
        
        public Node<T> getNext()
        {
            return myNext;
        }
    }
}
