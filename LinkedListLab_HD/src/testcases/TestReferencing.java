package testcases;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class TestReferencing 
{

	private LinkedList<String> myTestList = new LinkedList<String>();
	
	@Before
	public void setUp() throws Exception 
	{

	}

	@Test
	public void testContainsForObjectNotInList() 
	{
		assertFalse("Should have returned false", myTestList.contains("test"));
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertFalse("Should have returned false", myTestList.contains("test"));
	}
	
	@Test
	public void testContainsForRemovedObjects() 
	{
		assertFalse("Should have returned false", myTestList.contains("test"));
		myTestList.addFirst("test");
		assertTrue("Should have returned true", myTestList.contains("test"));
		myTestList.remove("test");
		assertFalse("Should have returned false", myTestList.contains("test"));
	}

	@Test
	public void testContainsForObjectsInList() 
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned true", myTestList.contains("Jim"));
		assertTrue("Should have returned true", myTestList.contains("Bob"));
		assertTrue("Should have returned true", myTestList.contains("Bill"));
	}
	
	@Test
	public void testContainsANullValue() 
	{
		myTestList.addFirst(null);
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertFalse("Should have returned false", myTestList.contains(null));
	}
	

	@Test
	public void testIndexOfObjectsInList()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned 0", myTestList.indexOf("Jim") == 0);
		assertTrue("Should have returned 1", myTestList.indexOf("Bob") == 1);
		assertTrue("Should have returned 2", myTestList.indexOf("Bill") == 2);
	}
	
	@Test
	public void testIndexOfMovedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned 0", myTestList.indexOf("Jim") == 0);
		assertTrue("Should have returned 1", myTestList.indexOf("Bob") == 1);
		assertTrue("Should have returned 2", myTestList.indexOf("Bill") == 2);
		myTestList.setFirst("Bill");
		assertTrue("Should have returned 0", myTestList.indexOf("Jim") == 1);
		assertTrue("Should have returned 1", myTestList.indexOf("Bob") == 2);
		assertTrue("Should have returned 2", myTestList.indexOf("Bill") == 0);
	}
	
	@Test
	public void testIndexOfDeletedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned 0", myTestList.indexOf("Jim") == 0);
		assertTrue("Should have returned 1", myTestList.indexOf("Bob") == 1);
		assertTrue("Should have returned 2", myTestList.indexOf("Bill") == 2);
		myTestList.remove("Bill");
		assertTrue("Should have returned 0", myTestList.indexOf("Jim") == 0);
		assertTrue("Should have returned 1", myTestList.indexOf("Bob") == 1);
		assertTrue("Should have returned -1", myTestList.indexOf("Bill") == -1);
		myTestList.remove("Jim");
		assertTrue("Should have returned -1", myTestList.indexOf("Jim") == -1);
		assertTrue("Should have returned 0", myTestList.indexOf("Bob") == 0);
		assertTrue("Should have returned -1", myTestList.indexOf("Bill") == -1);
	}
	
	@Test
	public void testIndexOfInsertedObjects() 
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		myTestList.insertBefore("Sara", "Bob");
		assertTrue("Should have returned 0", myTestList.indexOf("Jim") == 0);
		assertTrue("Should have returned 1", myTestList.indexOf("Sara") == 1);
		assertTrue("Should have returned 2", myTestList.indexOf("Bob") == 2);
		assertTrue("Should have returned 3", myTestList.indexOf("Bill") == 3);
	}
	
	@Test
	public void testSizeOfEmptyList() 
	{
		assertTrue("Should have returned 0", myTestList.size() == 0);
		myTestList.addFirst("Ricky");
		myTestList.remove("Ricky");
		assertTrue("Should have returned 0", myTestList.size() == 0);
	}
	
	@Test
	public void testSizeAfterAdding()
	{
		assertTrue("Should have returned 0", myTestList.size() == 0);
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned 3", myTestList.size() == 3);
		myTestList.addFirst("Becky");
		myTestList.addFirst("Sara");
		myTestList.addFirst("Rachel");
		assertTrue("Should have returned 6", myTestList.size() == 6);
	}
	
	@Test
	public void testSizeAfterRemoval()
	{
		assertTrue("Should have returned 0", myTestList.size() == 0);
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned 3", myTestList.size() == 3);
		myTestList.addFirst("Becky");
		myTestList.addFirst("Sara");
		myTestList.addFirst("Rachel");
		assertTrue("Should have returned 6", myTestList.size() == 6);
		myTestList.remove("Bill");
		myTestList.remove("Bob");
		myTestList.remove("Jim");
		assertTrue("Should have returned 3", myTestList.size() == 3);
	}
	
	@Test
	public void testSizeAfterMoving()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned 3", myTestList.size() == 3);
		myTestList.setFirst("Bill");
		assertTrue("Should have returned 3", myTestList.size() == 3);
	}
	
	@Test
	public void testGetFirstOnEmptyList()
	{
		assertTrue("Should have returned null", myTestList.getFirst() == null);
		myTestList.addFirst("test");
		myTestList.removeFirst();
		assertTrue("Should have returned null", myTestList.getFirst() == null);
	}
	
	@Test
	public void testGetFirstOnInsertedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned Jim", myTestList.getFirst().equals("Jim"));
		myTestList.insertBefore("Rachel", "Jim");
		assertTrue("Should have returned Rachel", myTestList.getFirst().equals("Rachel"));
	}
	
	@Test
	public void testGetFirstOnMovedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned Jim", myTestList.getFirst().equals("Jim"));
		myTestList.setFirst("Bob");
		assertTrue("Should have returned Bob", myTestList.getFirst().equals("Bob"));
	}
	
	@Test
	public void testGetFirstOnRemovedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned Jim", myTestList.getFirst().equals("Jim"));
		myTestList.removeFirst();
		myTestList.remove("Bob");
		assertTrue("Should have returned Bill", myTestList.getFirst().equals("Bill"));
	}
	
	@Test
	public void testGetLastOnEmptyList()
	{
		assertTrue("Should have returned null", myTestList.getLast() == null);
		myTestList.addLast("test");
		myTestList.removeFirst();
		assertTrue("Should have returned null", myTestList.getLast() == null);
	}
	
	@Test
	public void testGetLastOnAddedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned Jim", myTestList.getLast().equals("Bill"));
		myTestList.addLast("Rachel");
		assertTrue("Should have returned Rachel", myTestList.getLast().equals("Rachel"));
	}
	
	@Test
	public void testGetLastOnInsertedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned Jim", myTestList.getLast().equals("Bill"));
		myTestList.insertBefore("Rachel", null);
		assertTrue("Should have returned Rachel", myTestList.getLast().equals("Rachel"));
	}
	
	@Test
	public void testGetLastOnRemovedObjects()
	{
		myTestList.addFirst("Bill");
		myTestList.addFirst("Bob");
		myTestList.addFirst("Jim");
		assertTrue("Should have returned Bill", myTestList.getLast().equals("Bill"));
		myTestList.removeLast();
		myTestList.remove("Bob");
		assertTrue("Should have returned Jim", myTestList.getLast().equals("Jim"));
	}
}
