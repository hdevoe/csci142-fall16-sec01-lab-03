package testcases;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import structures.LinkedList;

public class TestRemoval 
{

	LinkedList<String> myTestList = new LinkedList<String>();

	@Before
	public void setUp() throws Exception 
	{
		myTestList.addFirst("Rachel");
		myTestList.addFirst("Marissa");
		myTestList.addFirst("Lauren");
		myTestList.addFirst("Sweeney");
		myTestList.addFirst("Porter");
		myTestList.addFirst("Matthew");

	}

	
	@Test
	public void testRemoveNonExistentObject() 
	{
		assertFalse("Should have returned false", myTestList.remove("Hayden"));
		assertFalse("Should have returned false", myTestList.remove("porter"));
		assertFalse("Should have returned false", myTestList.remove(" Matthew "));
	}
	
	@Test
	public void testRemoveObjectFromMiddle() 
	{
		assertTrue("Should have been true", myTestList.contains("Sweeney"));
		assertTrue("Should have been true", myTestList.remove("Sweeney"));
		assertFalse("Should have been false", myTestList.contains("Sweeney"));
		assertTrue("Should have been true", myTestList.size() == 5);
	}
	
	@Test
	public void testRemoveObjectsFromFrontAndEndUsingRemoveObject() 
	{
		assertTrue("Should have been true", myTestList.contains("Rachel"));
		assertTrue("Should have been true", myTestList.contains("Matthew"));
		assertTrue("Should have been true", myTestList.remove("Rachel"));
		assertTrue("Should have been true", myTestList.remove("Matthew"));
		assertFalse("Should have been false", myTestList.contains("Matthew"));
		assertFalse("Should have been false", myTestList.contains("Rachel"));
		assertTrue("Should have been true", myTestList.size() == 4);
	}
	
	@Test
	public void testRemoveAlreadyRemovedObject() 
	{
		assertTrue("Should have been true", myTestList.contains("Porter"));
		assertTrue("Should have been true", myTestList.remove("Porter"));
		assertFalse("Should have been false", myTestList.contains("Porter"));
		assertFalse("Should have been false", myTestList.remove("Porter"));
		assertFalse("Should have been false", myTestList.contains("Porter"));
	}
	
	
	
	
	@Test
	public void testRemoveFirstOnEmptyList() 
	{
		LinkedList<String> myTestList2 = new LinkedList<String>();
		assertTrue("Should have returned null", myTestList2.removeFirst()== null);
	}
	
	@Test
	public void testRemoveFirstOnInsertedObjects() 
	{
		assertTrue("Should have been Matthew", myTestList.getFirst().equals("Matthew"));
		myTestList.insertBefore("Hayden", "Matthew");
		assertTrue("Should have been Hayden", myTestList.getFirst().equals("Hayden"));
		assertTrue("Should have been Hayden", myTestList.removeFirst().equals("Hayden"));
		assertTrue("Should have been Matthew", myTestList.getFirst().equals("Matthew"));
	}
	
	@Test
	public void testRemoveFirstUntilListIsEmpty() 
	{
		assertTrue("Should have been Matthew", myTestList.removeFirst().equals("Matthew"));
		assertTrue("Should have been Porter", myTestList.removeFirst().equals("Porter"));
		assertTrue("Should have been Sweeney", myTestList.removeFirst().equals("Sweeney"));
		assertTrue("Should have been Lauren", myTestList.removeFirst().equals("Lauren"));
		assertTrue("Should have been Marissa", myTestList.removeFirst().equals("Marissa"));
		assertTrue("Should have been Rachel", myTestList.removeFirst().equals("Rachel"));
		assertTrue("Should have been null", myTestList.removeFirst() == null);
	}
	
	@Test
	public void testRemoveFirstOnMovedObjects() 
	{
		assertTrue("Should have been Matthew", myTestList.removeFirst().equals("Matthew"));
		myTestList.setFirst("Rachel");
		assertTrue("Should have been Rachel", myTestList.removeFirst().equals("Rachel"));
	}
	
	
	
	@Test
	public void testRemoveLastOnEmptyList() 
	{
		LinkedList<String> myTestList2 = new LinkedList<String>();
		assertTrue("Should have returned null", myTestList2.removeLast()== null);
	}
	
	@Test
	public void testRemoveLastOnInsertedObjects() 
	{
		assertTrue("Should have been Rachel", myTestList.getLast().equals("Rachel"));
		myTestList.insertBefore("Hayden", null);
		assertTrue("Should have been Hayden", myTestList.getLast().equals("Hayden"));
		assertTrue("Should have been Hayden", myTestList.removeLast().equals("Hayden"));
		assertTrue("Should have been Rachel", myTestList.getLast().equals("Rachel"));
	}
	
	@Test
	public void testRemoveLastUntilListIsEmpty() 
	{
		assertTrue("Should have been Rachel", myTestList.removeLast().equals("Rachel"));
		assertTrue("Should have been Marissa", myTestList.removeLast().equals("Marissa"));
		assertTrue("Should have been Lauren", myTestList.removeLast().equals("Lauren"));
		assertTrue("Should have been Sweeney", myTestList.removeLast().equals("Sweeney"));
		assertTrue("Should have been Porter", myTestList.removeLast().equals("Porter"));
		assertTrue("Should have been Matthew", myTestList.removeLast().equals("Matthew"));
		assertTrue("Should have been null", myTestList.removeLast() == null);
	}
	
	@Test
	public void testRemoveLastOnReplacedObjects() 
	{
		assertTrue("Should have been Rachel", myTestList.removeLast().equals("Rachel"));
		myTestList.addLast("Rachel");
		assertTrue("Should have been Rachel", myTestList.removeLast().equals("Rachel"));
	}
}
