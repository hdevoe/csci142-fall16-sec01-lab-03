package testcases;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class TestSetting {

	LinkedList<String> myTestList = new LinkedList<String>();

	@Before
	public void setUp() throws Exception 
	{
		myTestList.addFirst("Rachel");
		myTestList.addFirst("Marissa");
		myTestList.addFirst("Lauren");
		myTestList.addFirst("Sweeney");
		myTestList.addFirst("Porter");
		myTestList.addFirst("Matthew");

	}

	@Test
	public void testSetFirstInEmptyList() {
		LinkedList<String> myTestList2 = new LinkedList<String>();
		myTestList2.setFirst("test");
		assertFalse("Should not be able to set first on an empty list", myTestList2.getFirst() != null);
	}

	@Test
	public void testSetNonExistentObjectFirst() {
		myTestList.setFirst("Hayden");
		assertFalse("Should have returned false", myTestList.getFirst().equals("Hayden"));
		myTestList.setFirst("porter");
		assertFalse("Should have returned false", myTestList.getFirst().equals("porter"));
		 myTestList.remove(" Matthew ");
		assertFalse("Should have returned false", myTestList.getFirst().equals(" Matthew "));
	}
	
	@Test
	public void testFirstObjectAsFirst() {
		assertTrue("First object should be Matthew", myTestList.getFirst().equals("Matthew"));
		myTestList.setFirst("Matthew");
		assertTrue("First object should still be Matthew", myTestList.getFirst().equals("Matthew"));
		myTestList.setFirst("Matthew");
		assertTrue("First object should also still be Matthew", myTestList.getFirst().equals("Matthew"));
	}
	
	@Test
	public void testLastObjectAsFirst() {
		assertTrue("First object should be Matthew", myTestList.getFirst().equals("Matthew"));
		assertTrue("Last object should be Rachel", myTestList.getLast().equals("Rachel"));
		myTestList.setFirst("Rachel");
		assertTrue("First object should be Rachel", myTestList.getFirst().equals("Rachel"));
		assertTrue("Second object should be Matthew", myTestList.indexOf("Matthew") == 1);
		assertTrue("Last object should be Marissa", myTestList.getLast().equals("Marissa"));
	}
	
	@Test
	public void testMultipleObjectsInARowAsFirst() {
		assertTrue("First object should be Matthew", myTestList.getFirst().equals("Matthew"));
		myTestList.setFirst("Marissa");
		assertTrue("First object should be Marissa", myTestList.getFirst().equals("Marissa"));
		myTestList.setFirst("Porter");
		assertTrue("First object should be Porter", myTestList.getFirst().equals("Porter"));
		myTestList.setFirst("Rachel");
		assertTrue("First object should be Rachel", myTestList.getFirst().equals("Rachel"));
		myTestList.setFirst("Sweeney");
		assertTrue("First object should be Sweeney", myTestList.getFirst().equals("Sweeney"));
	}
	
	
}
