package testcases;

import static org.junit.Assert.*;

import org.junit.Test;

import structures.LinkedList;

public class TestGenerics {



	@Test
	public void testIntegerCapabilities() {
		LinkedList<Integer> myTestList = new LinkedList<Integer>();
		myTestList.addFirst(1);
		myTestList.addFirst(2);
		myTestList.addFirst(3);
		assertTrue("Should have been 1", myTestList.removeLast().equals(1));
		assertTrue("Should have been 2", myTestList.removeLast().equals(2));
		assertTrue("Should have been 3", myTestList.removeLast().equals(3));
	}
	
	@Test
	public void testDoubleCapabilities() {
		LinkedList<Double> myTestList = new LinkedList<Double>();
		myTestList.addFirst(1.0);
		myTestList.addFirst(2.0);
		myTestList.addFirst(3.0);
		assertTrue("Should have been 1", myTestList.removeLast().equals(1.0));
		assertTrue("Should have been 2", myTestList.removeLast().equals(2.0));
		assertTrue("Should have been 3", myTestList.removeLast().equals(3.0));
	}
	
	@Test
	public void testStringCapablities() {
		LinkedList<String> myTestList = new LinkedList<String>();
		myTestList.addFirst("One");
		myTestList.addFirst("Two");
		myTestList.addFirst("Three");
		assertTrue("Should have been 1", myTestList.removeLast().equals("One"));
		assertTrue("Should have been 2", myTestList.removeLast().equals("Two"));
		assertTrue("Should have been 3", myTestList.removeLast().equals("Three"));
	}
	
	@Test
	public void testObjectCapabilities() {
		LinkedList<LinkedList<Integer>> myTestList = new LinkedList<LinkedList<Integer>>();
		
		LinkedList<Integer> myTestListList1 = new LinkedList<Integer>();
		LinkedList<Integer> myTestListList2 = new LinkedList<Integer>();
		LinkedList<Integer> myTestListList3 = new LinkedList<Integer>();
		
		myTestList.addFirst(myTestListList1);
		myTestList.addFirst(myTestListList2);
		myTestList.addFirst(myTestListList3);
		assertTrue("Should have been 1", myTestList.removeLast().equals(myTestListList1));
		assertTrue("Should have been 2", myTestList.removeLast().equals(myTestListList2));
		assertTrue("Should have been 3", myTestList.removeLast().equals(myTestListList3));
	}
}
