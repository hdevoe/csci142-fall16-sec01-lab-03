package testcases;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class TestInsertions 
{

	LinkedList<String> myTestList = new LinkedList<String>();

	@Before
	public void setUp() throws Exception 
	{
		myTestList.addFirst("Rachel");
		myTestList.addFirst("Marissa");
		myTestList.addFirst("Lauren");
		myTestList.addFirst("Sweeney");
		myTestList.addFirst("Porter");
		myTestList.addFirst("Matthew");

	}

	@Test
	public void testInsertBeforeNullInEmptyList() {
		LinkedList<String> myTestList2 = new LinkedList<String>();
		assertTrue("Should have returned true", myTestList2.insertBefore("test", null));
		assertTrue("Should have returned true", myTestList2.getFirst().equals("test"));
	}
	
	@Test
	public void testInsertBeforeNonExistentNode() {
		assertFalse("Should have returned false", myTestList.insertBefore("test", "Hayden"));
		assertFalse("Should have returned false", myTestList.insertBefore("test", "porter"));
		assertFalse("Should have returned false", myTestList.insertBefore("test", " Matthew "));
	}
	
	@Test
	public void testInsertBeforeNullToInsertBehindLast() {
		assertTrue("Rachel should be last", myTestList.getLast().equals("Rachel"));
		assertTrue("Should have returned true", myTestList.insertBefore("test", null));
		assertTrue("test should be last", myTestList.getLast().equals("test"));
	}
	
	@Test
	public void testInsertBeforeExistingObject() {
		assertFalse("Should not contain test", myTestList.contains("test"));
		assertTrue("Index of Lauren should be 3", myTestList.indexOf("Lauren") == 3);
		assertTrue("Index of Sweeney should be 2", myTestList.indexOf("Sweeney") == 2);
		assertTrue("Should have been able to insert before Lauren", myTestList.insertBefore("test", "Lauren"));
		assertTrue("Should contain test", myTestList.contains("test"));
		assertTrue("Index of Lauren should be 4", myTestList.indexOf("Lauren") == 4);
		assertTrue("Index of test should be 3", myTestList.indexOf("test") == 3);
		assertTrue("Index of Sweeney should be 2", myTestList.indexOf("Sweeney") == 2);
	}
	
	@Test
	public void testInsertBeforeHead() {
		assertFalse("Should not contain test", myTestList.contains("test"));
		assertTrue("Matthew should be first", myTestList.getFirst().equals("Matthew"));
		assertTrue("Should have been able to insert before Matthew", myTestList.insertBefore("test", "Matthew"));
		assertTrue("test should be first", myTestList.getFirst().equals("test"));
		assertTrue("Index of Matthew should be 1", myTestList.indexOf("Matthew") == 1);
	}
	
	@Test
	public void testMultipleInsertions() {
		assertFalse("Should not contain test1", myTestList.contains("test1"));
		assertFalse("Should not contain test", myTestList.contains("test2"));
		assertFalse("Should not contain test", myTestList.contains("test3"));
		assertTrue("Should have been able to insert before Marissa", myTestList.insertBefore("test3", "Marissa"));
		assertTrue("Should have been able to insert before test3", myTestList.insertBefore("test2", "test3"));
		assertTrue("Should have been able to insert before test2", myTestList.insertBefore("test1", "test2"));
		assertTrue("Index of test1 should be 4", myTestList.indexOf("test1") == 4);
		assertTrue("Index of test2 should be 5", myTestList.indexOf("test2") == 5);
		assertTrue("Index of test3 should be 6", myTestList.indexOf("test3") == 6);
	}

}
