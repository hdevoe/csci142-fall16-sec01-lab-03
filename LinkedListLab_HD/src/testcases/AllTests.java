package testcases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestGenerics.class, TestInsertions.class, TestReferencing.class, TestRemoval.class, TestSetting.class })
public class AllTests {

}
